import Component from '@ember/component';
import { get, set } from '@ember/object';
import { argument } from '@ember-decorators/argument';
import { optional } from '@ember-decorators/argument/types';
import { computed, on } from '@ember-decorators/object';
import { attribute, tagName } from '@ember-decorators/component';
import { EKMixin, EKOnFocusMixin, keyDown, keyUp } from 'ember-keyboard';
import { localClassName } from 'ember-css-modules';
import { task, timeout } from 'ember-concurrency';

const ALLOWED_KEYS = /^[a-z0-9]$/i;
const SPECIAL_KEYS = ['Enter', 'Backspace'];
const FAST_TYPING_DEBOUNCE = 100;

@tagName('input')
export default class ComboPwInputComponent extends Component.extend(EKMixin, EKOnFocusMixin, {
  fastTypingDebounceTask: task(function * () {
    if (!get(this, 'fastTyping')) {
      return;
    }

    yield timeout(FAST_TYPING_DEBOUNCE);

    set(this, 'fastTyping', false);
  }).drop()
}) {

  @argument(Array)
  password = [];

  @argument(optional(Object))
  onSubmit() {}

  @argument(optional('string'))
  @attribute
  placeholder = '';

  @argument(optional('string'))
  @attribute
  type = 'password';

  @argument(optional('boolean'))
  readonly = false;

  @attribute
  autocomplete = 'off';

  @argument(optional('boolean'))
  @attribute
  required = true;

  @argument(optional('boolean'))
  @localClassName
  transparent = false;

  @argument(optional(Array))
  buffer = [];

  @argument(optional('number'))
  maxLength = 18;

  @attribute
  spellcheck = false;

  // used to prevent fast typing resulting in input of a key combination instead of single keypresses
  // false if the first key in the buffer was pressed longer than FAST_TYPING_DEBOUNCE
  fastTyping = true;

  @attribute()
  @computed('password.[]', 'buffer.[]')
  get value() {
    return this.password.map(combo => combo.map(keyPress => keyPress.key).join('')).join('') +
      this.buffer.map(keyPress => keyPress.key).join('');
  }

  @on(keyDown())
  keyPressed(event) {
    const key = event.key;
    const keyCode = event.keyCode;

    if (get(this, 'readonly')) {
      return;
    }

    const isAllowed = ALLOWED_KEYS.test(key);
    const isSpecialKey = SPECIAL_KEYS.some(special => special === key);

    if (isAllowed || isSpecialKey) {
      event.preventDefault();
    } else {
      return;
    }

    const buffer = get(this, 'buffer');
    const maxLength = get(this, 'maxLength');
    const currentLength = get(this, 'value').length;

    if ('Enter' === key && this.onSubmit) {
      this.onSubmit();
    } else if ('Backspace' === key) {
      this.deleteInput();
    } else if (currentLength + 1 > maxLength) {
      return;
    } else if (!buffer.some(keyPress => keyPress.keyCode === keyCode)) {
      buffer.pushObject({keyCode, key});

      if (buffer.length === 1) {
        // set fastTyping to false when the first key is pressed long enough
        get(this, 'fastTypingDebounceTask').perform();
      }
    }
  }

  @on(keyUp())
  keyReleased(event) {
    event.preventDefault();

    get(this, 'fastTypingDebounceTask').cancelAll();

    const buffer = get(this, 'buffer');

    if (buffer.length > 0) {
      const password = get(this, 'password');
      const fastTyping = get(this, 'fastTyping');

      if (fastTyping) { // all keys in the buffer are treated as single keypresses
        buffer.forEach(keyPress => {
          const singleKeypress = [keyPress];
          singleKeypress.string = keyPress.key;
          password.pushObject(singleKeypress);
        });
      } else { // the buffer contains a key combination
        buffer.string = buffer.map(keyPress => keyPress.key).join('');
        password.pushObject(buffer);

        set(this, 'fastTyping', true); // rearm fastTyping for the next input
      }

      set(this, 'buffer', []);
    }
  }

  deleteInput() {
    const password = get(this, 'password');
    password.popObject()
  }
}

